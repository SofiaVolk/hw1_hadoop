package mcr;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class ByteCounterMapper extends Mapper<Object, Text, Text, MapWritable> {
    /**
     class Mapper<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
         KEYIN: Object -> key
         VALUEIN: Text -> log file string
         KEYOUT: Text -> IP
         VALUEOUT: class MapWritable -> bytes total     // for passing multiple values
     */

    private static Text ip = new Text();
    MapWritable mw = new MapWritable();

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        // Parse the log line to get IP (the first field) and size of request in bytes (the last field)
        String [] input_request = value.toString().split("\\s");

//        bytes = new IntWritable(Integer.parseInt(input_request[9]));
        try{
            mw.put(new IntWritable(1), new IntWritable(Integer.parseInt(input_request[9])));
        }
        catch (Exception ex){
            return;
        }
        ip.set(input_request[0]);
        context.write(ip, mw);
    }
}