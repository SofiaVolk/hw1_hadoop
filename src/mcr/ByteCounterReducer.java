package mcr;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ByteCounterReducer extends Reducer<Text, MapWritable, Text, String> {
    /**
    class Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
        KEYIN: Text -> IP
        VALUEIN: class MapWritable -> multiple values(bytes total, number of requests)
        KEYOUT: Text -> IP
        VALUEOUT: String -> bytes average, bytes total
	*/

    static IntWritable GET_PARAM_1 = new IntWritable(1);
    static IntWritable GET_PARAM_2 = new IntWritable(2);

    public void reduce(Text key, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {
        int sum_bytes = 0;
        int sum_requests = 0;
        double average = 0;

        // Count total bytes and total number of requests by IP
        for (MapWritable value : values) {
            sum_bytes += ((IntWritable) value.get(GET_PARAM_1)).get();
            sum_requests += ((IntWritable) value.get(GET_PARAM_2)).get();
        }

        // Count average bytes per request by IP
        average = (double) sum_bytes / sum_requests;

        context.write(key, String.valueOf(average) + "," + String.valueOf(sum_bytes));
    }
}
