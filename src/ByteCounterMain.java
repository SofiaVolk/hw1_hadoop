import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import mcr.ByteCounterCombiner;
import mcr.ByteCounterReducer;
import mcr.ByteCounterMapper;

//a(2), b(1), c(6), d(1)

public class ByteCounterMain extends Configured implements Tool {

    public int run(String[] args) throws Exception {
        // Configurations for job
        Configuration conf = new Configuration();

        // Form csv output file
        conf.set("mapred.textoutputformat.separator", ",");
        Job job = Job.getInstance(conf, "Byte count");
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setJarByClass(ByteCounterMain.class);

        // Setting data key-value data types
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(MapWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        // Setting the Mapper, Combiner and Reducer classes
        job.setMapperClass(ByteCounterMapper.class);
        job.setCombinerClass(ByteCounterCombiner.class);
        job.setReducerClass(ByteCounterReducer.class);

        // Setting the ammount of reducers
        job.setNumReduceTasks(2);

        // Specify input and output directories
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return (job.waitForCompletion(true) ? 0 : 1);
    }

    public static void main(String[] args) throws Exception {
        // Running the job with configurations in conf
        int exitCode = ToolRunner.run(new ByteCounterMain(), args);
        System.exit(exitCode);
    }
}