def main():
    with open('test_log.txt', 'r') as file_from:
        temp = file_from.read()
    with open('log.txt', 'w') as file_to:
        for _ in range(10):
            file_to.write(temp)


if __name__ == "__main__":
    args = main()
