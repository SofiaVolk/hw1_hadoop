package mcr;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


import java.io.IOException;

public class ByteCounterCombiner extends Reducer<Text, MapWritable, Text, MapWritable> {
    /**
     class Combiner<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
     KEYIN: Text -> IP
     VALUEIN: MapWritable -> bytes total
     KEYOUT: Text -> IP
     VALUEOUT: MapWritable -> multiple values(bytes total, number of requests)
     */

    static IntWritable GET_PARAM = new IntWritable(1);
    private MapWritable mw = new MapWritable();

    public void reduce(Text key, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {
        int sum_bytes = 0;
        int sum_requests = 0;

        // Count total bytes by IP locally
        for (MapWritable value : values) {
            sum_bytes += ((IntWritable) value.get(GET_PARAM)).get();
            sum_requests += 1;
        }

        mw.put(new IntWritable(1), new IntWritable(sum_bytes));
        mw.put(new IntWritable(2), new IntWritable(sum_requests));
        context.write(key, mw);
    }
}